[![Bisect Hosting](https://www.bisecthosting.com/images/logos/dark_text@1538x500.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil)

If you want a server setup for the Any mod pack with zero effort, get a [server with BisectHosting](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil) and receive 25% off your first month as a new customer using the code kreezxil

## Description

Special blocks that will put into the world an item matching their type. Except in the case of Obsidian, that one will randomly generate a gold or iron ingot or a diamond.

Place the block in a crafting grid to get the next type.

You can also use an Iron Block, Nether Star, and Bed to create the Iron Generator.

Redstone signal to turn it off.

## Recipes

![Iron Generator](https://i.imgur.com/fiRrmks.png)
Iron Generator

![Gold Generator](https://i.imgur.com/zrTdRWl.png)

Gold Generator

![Diamond Generator](https://i.imgur.com/JS45C7c.png)

Diamond Generator

![Obsidian Generator](https://i.imgur.com/IdLLbbV.png)

Obsidian Generator

## Modpacks

Don't ask just add. A link back here would be nice but not required.

## Streams/Tubers

Yes, please. Share the stream and video link in the comments below and I'll add it to the overview. That's free advertising for you!

## How To Configure It

**This project is proudly powered by FORGE**, without whom it would not be possible. Help FORGE get rid of Adfocus at [https://www.patreon.com/LexManos](https://www.patreon.com/LexManos).
